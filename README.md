# Simple Blockchain in Go


## Deployment steps:
1. `git clone https://gitlab.com/SW55293/go-blockchain.git`
- navigate to this directory and rename the example file myexample.env
2. `go run main.go`
- open a web browser and visit http://localhost:8080/
    to write new blocks, send a POST request (Such as Postman or Insomnia) to http://localhost:8080/ with a JSON payload with BPM as the key and an  integer as the value.

Credit goes to ==> https://mycoralhealth.medium.com/code-your-own-blockchain-in-less-than-200-lines-of-go-e296282bcffc

